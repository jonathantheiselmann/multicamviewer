# MultiCamViewer
The main idea of this cinder block is to simplify displaying a 3D scene from multiple perspectives / sides simultaneously.

## Usage
The library is splitting up the window into a 2x2 grid (4 views) and position an individual camera in each of them. By default it uses a perspective camera for the upper-left and orthographic cameras for the other 3 viewports as it is known by most 3D tools like MAXON Cinema4D.

* upper-left: Perspective
* upper-right: Top
* lower-left: Left
* lower-right: Front

The camera and the camera settings of each viewport can be changed during running time. This will allow rendering the same view in each segment of the 2x2 grid, but using different camera and render settings (f.e. wireframe).

Each viewport is using a callback wrapper function in the draw() function. The to be rendered scene can live in the main application file or anywhere in the project code.
This gives the user full flexibility and control over the rendering process. It also allows to render 4 completely different scenes in each viewport.

Simply bind your function that renders your scene the each viewport individually.

```
for ( auto viewPort : multiCamViewer->getViewPorts() ) {
	viewPort->setDraw( std::bind( &YourApp::yourScene, this ) );
}
```

The library is using the same keyboard shortcuts as MAXON Cinema4D to switch between fullscreen and splitscreen mode

* F1 -> set upper-left viewport in fullscreen mode
* F2 -> set upper-right viewport in fullscreen mode
* F3 -> set lower-left viewport in fullscreen mode
* F4 -> set lower-right viewport in fullscreen mode
* F5 -> set all viewports in splitscreen mode

## Example
### BasicApp_V2
BasicApp_V2 is a basic example rendering the same 3D scene in each viewport with the default configuration using a perspective camera and 3 orthographic cameras.

### MultiScenes
MultiScnes showcases rendering different scenes to the 4 viewports and changing the default camera per viewport.