#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

#include "MultiCamViewer.h"

using namespace ci;
using namespace ci::app;
using namespace std;

using namespace idt;

class MultiScenesApp : public App {
  public:
	void setup() override;
	void update() override;
	void draw() override;
    
    void renderSceneA();
    void renderSceneB();
    
    MultiCamViewerRef multiCamViewer;
    gl::BatchRef shapeA;
    gl::BatchRef shapeB;
};

void MultiScenesApp::setup()
{
    // setup MultiCamViewer
    multiCamViewer = idt::MultiCamViewer::create();
    
    // setup individual callback function for each viewPort
    int index = 0;
    for ( auto v : multiCamViewer->getViewPorts() ) {
        if (index < 2) {
            v->setDraw( std::bind( &MultiScenesApp::renderSceneA, this ));
            v->setWireframe( true );
        }
        else {
            v->setDraw( std::bind( &MultiScenesApp::renderSceneB, this ));
            if ( index == 2 ) {
                v->setView( ViewPort::PERSPECTIVE );
                v->setup();
            }
            else if ( index == 3) {
                v->setView( ViewPort::TOP );
                v->setup();
            }
        }
        index++;
    }
    
    // setup shader
    auto lambert = gl::ShaderDef().lambert().color();
    gl::GlslProgRef shader = gl::getStockShader( lambert );
    
    // setup BatchRef
    shapeA = gl::Batch::create( geom::Teapot(), shader );
    shapeB = gl::Batch::create( geom::Cube(), shader );
}


void MultiScenesApp::update()
{
}

void MultiScenesApp::draw()
{
    gl::clear( Color( 0.1, 0.1, 0.1 ) );
    
    // draw viewports
    multiCamViewer->draw();
    
    // display FPS as window title
    getWindow()->setTitle(to_string(getAverageFps()));
}

void MultiScenesApp::renderSceneA() {
    {
        gl::enableDepthRead();
        gl::enableDepthWrite();
        
        shapeA->draw();
    }
}

void MultiScenesApp::renderSceneB() {
    {
        gl::enableDepthRead();
        gl::enableDepthWrite();
        
        shapeB->draw();
    }
}

CINDER_APP( MultiScenesApp, RendererGl )
