#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

#include "MultiCamViewer.h"

using namespace ci;
using namespace ci::app;
using namespace std;

using namespace idt;

class BasicApp_V2 : public App {
public:
    void setup() override;
    void update() override;
    void draw() override;
    
    void render();
    
    MultiCamViewerRef multiCamViewer;
    gl::BatchRef shape;
};

void BasicApp_V2::setup()
{
    // setup MultiCamViewer
    multiCamViewer = idt::MultiCamViewer::create();
    
    // setup individual callback function for each viewPort
    for ( auto v : multiCamViewer->getViewPorts() ) {
        v->setDraw( std::bind( &BasicApp_V2::render, this ) );
    }
    
    // setup shader
    auto lambert = gl::ShaderDef().lambert().color();
    gl::GlslProgRef shader = gl::getStockShader( lambert );
    
    // setup BatchRef
    shape = gl::Batch::create( geom::Cube(), shader );
}

void BasicApp_V2::update() {
    
}

void BasicApp_V2::draw() {
    gl::clear( Color( 0.1, 0.1, 0.1 ) );
    
    // draw viewports
    multiCamViewer->draw();
    
    // display FPS as window title
    getWindow()->setTitle(to_string(getAverageFps()));
}

void BasicApp_V2::render() {
    {
        gl::enableDepthRead();
        gl::enableDepthWrite();
        
        int numSpheres = 64;
        float maxAngle = M_PI * 7;
        float spiralRadius = 1;
        float height = 2;
        float boxSize = 0.05f;
        float anim = getElapsedFrames() / 30.0f;
        
        for( int s = 0; s < numSpheres; ++s ) {
            float rel = s / (float)numSpheres;
            float angle = rel * maxAngle;
            float y = fabs( cos( rel * M_PI + anim ) ) * height;
            float r = rel * spiralRadius;
            vec3 offset( r * cos( angle ), y / 2, r * sin( angle ) );
            
            gl::pushModelMatrix();
            gl::translate( offset );
            gl::scale( vec3( boxSize, y, boxSize ) );
            gl::color( Color( CM_HSV, rel, 1, 1 ) );
            shape->draw();
            gl::popModelMatrix();
        }
    }
    
    gl::disableDepthRead();
}

CINDER_APP( BasicApp_V2, RendererGl, []( BasicApp_V2::Settings *settings ) {
    settings->setWindowSize( 1440, 1080 );
    settings->setFrameRate(60);
})