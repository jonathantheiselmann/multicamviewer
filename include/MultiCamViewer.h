//
//  MultiCamViewer.h
//
//
//  Created by Moritz Theiselmann on 04/04/18.
//
//

#pragma once

#include "ViewPort.h"

using namespace ci;
using namespace ci::app;
using namespace std;

namespace idt {
    
    typedef std::shared_ptr<class MultiCamViewer> MultiCamViewerRef;
    
    class MultiCamViewer {
    public:
        MultiCamViewer() {
            dimensions = getWindowSize();
            position = vec2( 0 );

            fullscreen = false;
            disableKeyEventListener = false;
            showInterfaces = true;
            setup();
        }
        
        MultiCamViewer( vec2 _dimensions, vec2 _position ) {
            dimensions = _dimensions;
            position = _position;
            
            fullscreen = false;
            disableKeyEventListener = false;
            setup();
        }
        
        ~MultiCamViewer() {
            viewPorts.clear();
        }
        
        void setup() {
            float width = dimensions.x / 2;
            float height = dimensions.y / 2;
            
            // set up upper-left viewport + set it as "PERSPECTIVE" by default
            {
                ViewPortRef viewPort = ViewPort::create( ViewPort::PERSPECTIVE );
                viewPort->setWireframe( false );
                viewPort->setDimensions( ivec2( width, height ) );
                viewPort->setPosition( ivec2( 0, 0 ) );
                viewPort->setActive( true );
                viewPort->setLowerLeft( ivec2( 0, height ) );
                viewPort->setupGui( "upper-left-gui" );
                
                viewPorts.push_back(viewPort);
            }
            
            // set up upper-right viewport + set it as "TOP" by default
            {
                ViewPortRef viewPort = ViewPort::create( ViewPort::TOP);
                viewPort->setWireframe( false );
                viewPort->setDimensions( ivec2( width, height ) );
                viewPort->setPosition( ivec2( width, 0 ) );
                viewPort->setLowerLeft( ivec2( width, height ) );
                viewPort->setupGui( "upper-right-gui" );
                
                viewPorts.push_back(viewPort);
            }
            
            // set up lower-left viewport + set it as "LEFT" by default
            {
                ViewPortRef viewPort = ViewPort::create( ViewPort::LEFT );
                viewPort->setWireframe( false );
                viewPort->setDimensions( ivec2( width, height ) );
                viewPort->setPosition( ivec2( 0, height ) );
                viewPort->setLowerLeft( ivec2( 0, 0 ) );
                viewPort->setupGui( "lower-left-gui" );
                
                viewPorts.push_back(viewPort);
            }
            
            // set up lower-right viewport + set it as "FRONT" by default
            {
                ViewPortRef viewPort = ViewPort::create( ViewPort::FRONT );
                viewPort->setWireframe( false );
                viewPort->setDimensions( ivec2( width, height ) );
                viewPort->setPosition( ivec2( width, height ) );
                viewPort->setLowerLeft( ivec2( width, 0 ) );
                viewPort->setupGui( "lower-right-gui" );
                
                viewPorts.push_back(viewPort);
            }
            
            for ( auto v : viewPorts ) {
                 v->configCamera();
            }
            
            // setup event listener
            getWindow()->getSignalKeyUp().connect( std::bind( &MultiCamViewer::keyUpHandler, this, std::placeholders::_1 ) );
//            getWindow()->getSignalResize().connect( std::bind( &MultiCamViewer::resizeHandler, this ) );
        }
        
        static MultiCamViewerRef create() {
            return std::make_shared<MultiCamViewer>();
        }
        
        static MultiCamViewerRef create( vec2 _dimensions, vec2 _position ) {
            return std::make_shared<MultiCamViewer>( _dimensions, _position );
        }
        
        // keep track of window width and height
        // and adjust camera viewports accordingly
        void resizeHandler( ) {
            float width = getWindowWidth() / 2;
            float height = getWindowHeight() / 2;
//            float width = dimensions.x / 2;
//            float height = dimensions.y / 2;

            int index = 0;
            for ( auto v : viewPorts ) {
                v->setDimensions( ivec2( width, height ) );
                if ( index == 0 ) {
                    v->setPosition( ivec2( 0, 0 ) );
                    v->setLowerLeft( ivec2( 0, height ) );
                }
                else if ( index == 1 ) {
                    v->setPosition( ivec2( width, 0 ) );
                    v->setLowerLeft( ivec2( width, height ) );
                }
                else if ( index == 2 ) {
                    v->setPosition( ivec2( 0, height ) );
                    v->setLowerLeft( ivec2( 0, 0 ) );
                }
                else if ( index == 3 ) {
                    v->setPosition( ivec2( width, height ) );
                    v->setLowerLeft( ivec2( width, 0 ) );
                }

                // reconfigure viewport camera settings
                v->configCamera();
                index++;
            }
        }
        
        void draw() {
            // draw viewports
            for ( auto v : viewPorts ) {
                v->draw();
            }
            
            // draw viewport separators
            {
                if ( !fullscreen ) {
                    float width = dimensions.x / 2;
                    float height = dimensions.y / 2;
                    
                    gl::ScopedColor scpColor( 1, 1, 1 );
                    
                    gl::drawLine( ivec2( width, 0 ), ivec2( width, height * 2 ) );
                    gl::drawLine( ivec2( 0, height ), ivec2( width * 2, height ) );
                }
            }
        }
        
        // listening to key events
        // swichting between full screen and split screen
        void keyUpHandler( KeyEvent event ) {
            if ( !disableKeyEventListener ) {
                switch( event.getCode() ) {
                    case KeyEvent::KEY_F1:
                        // f1 -> first viewport (upper-left) in fullscreen
                        selectFullscreen( 0 );
                        fullscreen = true;
                        break;
                    case KeyEvent::KEY_F2:
                        // f2 -> second viewport (upper-right) in fullscreen
                        selectFullscreen( 1 );
                        fullscreen = true;
                        break;
                    case KeyEvent::KEY_F3:
                        // f3 -> third viewport (lower-left) in fullscreen
                        selectFullscreen( 2 );
                        fullscreen = true;
                        break;
                    case KeyEvent::KEY_F4:
                        // f4 -> forth viewport (lower-right) in fullscreen
                        selectFullscreen( 3 );
                        fullscreen = true;
                        break;
                    case KeyEvent::KEY_F5:
                        // f5 -> split screen
                        selectSplitScreen();
                        fullscreen = false;
                        break;
                    default:
                        break;
                }
            }
        }
        
        void selectSplitScreen() {
            for ( auto v : viewPorts ) {
                v->setDisplayMode( ViewPort::SPLITSCREEN );
                v->setActive( false );
            }
        }
        
        void selectFullscreen( int index) {
            int loopIndex = 0;
            for ( auto v : viewPorts ) {
                if ( loopIndex == index ) {
                    v->setDisplayMode( ViewPort::FULLSCREEN );
                    v->setActive( true );
                }
                else {
                    v->setDisplayMode( ViewPort::NONE );
                    v->setActive( false );
                }
                loopIndex++;
            }
        }
        
        //////////////////////////////////////////////////////////////////////
        
        // get methods
        vector<ViewPortRef> getViewPorts() {
            return viewPorts;
        }
        
        bool getDisableKeyEventListner() {
            return disableKeyEventListener;
        }
        
        bool getShowInterfaces() {
            return showInterfaces;
        }
        
        // set methods
        void setDisableKeyEventListener( bool _disableKeyEventListener ) {
            disableKeyEventListener = _disableKeyEventListener;
        }
        
        void setFullscreen( bool _fullscreen ) {
            fullscreen = _fullscreen;
        }
        
        void setShowInterfaces( bool _showInterfaces ) {
            showInterfaces = _showInterfaces;
            for ( auto v : viewPorts ) {
                v->setShowGui( !v->getShowGui() );
            }
        }
        
    protected:
        vector<ViewPortRef> viewPorts;
        bool disableKeyEventListener;
        bool fullscreen;
        bool showInterfaces;
        
        vec2 dimensions;
        vec2 position;
    };
    
}
