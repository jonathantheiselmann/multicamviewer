//
//  ViewPort.h
//  
//
//  Created by Moritz Theiselmann on 04/04/18.
//
//

#pragma once

#include "cinder/CameraUi.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"

using namespace ci;
using namespace ci::app;
using namespace std;
using namespace params;

namespace idt {
    typedef std::shared_ptr<class ViewPort> ViewPortRef;
    
    class ViewPort {
    public:
        // camera view
        enum VIEW {
            PERSPECTIVE,
            TOP,
            LEFT,
           /* RIGHT,*/
            FRONT/*,
           BACK*/
        };
        
        // display modes
        enum DISPLAY {
            NONE,
            FULLSCREEN,
            SPLITSCREEN,
        };
        
        // camera settings
        struct cameraSettings {
            string title;
            float size;
            vec3 eyePoint;
            vec3 target;
            vec3 up;
        } camSetting;
        
        // constructor
        ViewPort( VIEW _view ) {
            // list of views (enum) for params InterfaceGl
            if ( enumNames.size() == 0 ) {
                enumNames.push_back( "PERSPECTIVE" );
                enumNames.push_back( "TOP" );
                enumNames.push_back( "LEFT" );
                enumNames.push_back( "FRONT" );
            }
            
            view = _view;
            display = SPLITSCREEN;
            showGui = true;
            
            setup();
        }
        
        // destructor
        ~ViewPort() {
            
        }
        
        // config event listener
        void configureEvents() {
            getWindow()->getSignalMouseDown().connect(std::bind( &ViewPort::mouseDownHandler, this, std::placeholders::_1 ) );
            getWindow()->getSignalMouseUp().connect( std::bind( &ViewPort::mouseUpHandler, this, std::placeholders::_1 ) );
            getWindow()->getSignalMouseDrag().connect( std::bind( &ViewPort::mouseDragHandler, this, std::placeholders::_1 ) );
        }
        
        static ViewPortRef create( VIEW _view ) {
            return std::make_shared<ViewPort>( _view );
        }
        
        // configure camera settings
        void setup() {
            switch( view ) {
                case PERSPECTIVE:
                    camSetting.title = "PERSPECTIVE";
                    camSetting.size = 5.0f;
                    camSetting.eyePoint = vec3( 0, 0, camSetting.size );
                    camSetting.target = vec3( 0 );
                    camSetting.up = vec3( 0, 1, 0 );
                    break;
                
                case TOP:
                    camSetting.title = "TOP";
                    camSetting.size = 5.0f;
                    camSetting.eyePoint = vec3( 0, camSetting.size, 0 );
                    camSetting.target = vec3( 0 );
                    camSetting.up = vec3( 0, 0, -1 );
                    break;
                    
                case LEFT:
                    camSetting.title = "LEFT";
                    camSetting.size = 5.0f;
                    camSetting.eyePoint = vec3( -camSetting.size, 0, 0 );
                    camSetting.target = vec3( 0 );
                    camSetting.up = vec3( 0, 1, 0 );
                    break;
                
                /*case RIGHT:
                    // TO DO
                    
                    break;*/
                    
                case FRONT:
                    camSetting.title = "FRONT";
                    camSetting.size = 5.0f;
                    camSetting.eyePoint = vec3( 0, 0, camSetting.size );
                    camSetting.target = vec3( 0 );
                    camSetting.up = vec3( 0, 1, 0 );
                    break;
                    
                /*case BACK:
                    // TO DO
                    break;*/
                
                default:
                    // TO DO
                    break;
            }
            
            configCamera();
            configureEvents();
            setupGrid();
        }
        
        // set up GUI for individual viewport
        void setupGui( string _title ) {
            if ( !gui ) {
                gui = InterfaceGl::create( getWindow(), _title, toPixels( ivec2( 200, 200 ) ) );
                
                gui->addParam( "View Port", enumNames, (int*)&view )
                    .updateFn( [this] {
                        setup();
                    } ).group( "Camera" );
                
                gui->addSeparator();
                
                gui->addParam( "Wireframe", &wireFrame ).group( "View" );
                gui->addParam( "Origin", &drawOrigin ).group( "View" );
                gui->addParam( "Grid", &drawGrid ).group( "View" );
                
                gui->addSeparator();
                
                gui->addParam( "Title", &drawTitle ).group( "General" );
                
                gui->addSeparator();
                
            }
        }
        
        // config orthographic / perspective camera based on selected view
        void configCamera() {
            if ( view == PERSPECTIVE ) {
                camPersp.setPerspective( 60.0f, aspectRatio, 0.1f, 1000.0f ); // 30.0f
                camPersp.lookAt( camSetting.eyePoint, camSetting.target, camSetting.up );
                camUi = CameraUi( &camPersp );
            }
            else {
                camOrtho.setOrtho(-camSetting.size * aspectRatio, camSetting.size * aspectRatio, -camSetting.size, camSetting.size, 0.1f, 1000.0f);
                camOrtho.lookAt(camSetting.eyePoint, camSetting.target, camSetting.up);
            }
        }
        
        void draw() {
            
            ivec2 tmpPos = position;
            ivec2 tmpDimensions = dimensions;
            
            // temporarily adjust viewport size and viewport position if display mode "PERSPECITVE"m
            if ( display == FULLSCREEN ) {
                tmpPos = ivec2( 0 );
                tmpDimensions = dimensions*2;
            }
            
            {
                // hide GUI and don't draw anything else on display mode "NONE"
                if ( display == NONE ) {
                    if ( gui ) {
                        gui->hide();
                    }
                    return;
                }
                else {
                    if ( gui ) {
                        gui->show();
                    }
                }
                
                
                gl::pushMatrices();

                gl::translate( tmpPos.x, tmpPos.y );
                
                // draw viewport title
                if ( drawTitle ) {
                    int offset = 10;
                    gl::drawString( camSetting.title, ivec2( 0 + offset, 0 + offset ), Color(1, 0.5, 0.5) );
                }
                
                // and highlight selected viewport
                if ( active ) {
                    gl::ScopedColor scpColor( 1.0, 0.5, 0.5 );
                    gl::drawStrokedRect( Rectf( 2, 2, tmpDimensions.x-2, tmpDimensions.y-2 ) );
                }
                
                gl::popMatrices();
            }
            
            // draw 3d scene
            {
                if ( display == FULLSCREEN ) {
                    gl::pushViewport( ivec2( 0 ), dimensions*2 );
                }
                else {
                    gl::pushViewport( lowerLeft, dimensions );
                }
                
                gl::pushMatrices();
                if ( view == PERSPECTIVE ) {
                    gl::setMatrices( camPersp );
                }
                else {
                    gl::setMatrices( camOrtho );
                }
                
                // disable depthread before drawing the grid and origin
                gl::disableDepthRead();
                
                if ( drawOrigin ) {
                    gl::drawCoordinateFrame();
                }
                
                if ( drawGrid ) {
                    if ( horizontalPlaneBatch ) {
                        horizontalPlaneBatch->draw();
                    }
                    if ( verticalPlaneBatch ) {
//                        verticalPlaneBatch->draw();
                    }
                }
                
                // draw 3d meshes as wireframe
                if ( wireFrame ) {
                    gl::enableWireframe();
                }
                
                // callback -> display 3d content
                if (drawCallback) {
                    drawCallback();
                }
                
                // disable wireframe mode
                if (gl::isWireframeEnabled()) {
                     gl::disableWireframe();
                }
                
                gl::popMatrices();
                gl::popViewport();
            }
            {
                gl::translate( tmpPos.x, tmpPos.y );
                
                gl::pushMatrices();
                gl::translate( tmpPos.x, tmpPos.y );
                
                if ( gui && showGui ) {
                    // temporary workaround to disable GUI reposition (drag) feature
                    ivec2 guiPos = tmpPos;
                    guiPos.x += tmpDimensions.x - gui->getSize().x;
                    gui->setPosition( guiPos );
                    
                    gui->draw();
                }
                
                gl::popMatrices();
            }
        }
        
        // callback wrapper function
        void setDraw(std::function<void()> drawFn) {
            drawCallback = drawFn;
        }
        
        std::function<void()> drawCallback;
        
        // to do:
        // configure different planes for each view
        void setupGrid() {
            auto color = gl::ShaderDef().color();
            auto shader = gl::getStockShader( color );
            
            // create 2 planes
            int subdivisions = 30;
            float size = 15;
            
            auto horizontalPlane = geom::WirePlane().subdivisions( ivec2( subdivisions, subdivisions ) ).size( vec2( size, size ) ).origin( vec3( 0, 0, 0 ) );
//            auto verticalPlane = geom::WirePlane().axes( vec3( 1, 0, 0 ), vec3( 0, 1, 0 ) ).subdivisions( ivec2( subdivisions, subdivisions ) ).size( vec2( size, size ) ).origin( vec3( 0, size / 2, 0 ) );
            auto white = geom::Constant( geom::COLOR, ColorA( 1, 1, 1, 0.2 ) );
            
            horizontalPlaneBatch = gl::Batch::create( horizontalPlane >> white, shader );
//            verticalPlaneBatch = gl::Batch::create( verticalPlane >> white, shader );
        }
        
        //////////////////////////////////////////////////////////////////////
        
        // select viewport on mouseUp based on mouse position
        // only enable camera navigation for selected viewport
        void mouseUpHandler ( MouseEvent event ) {
            ivec2 from;
            ivec2 to;
            
            if ( display == FULLSCREEN ) {
                to = dimensions*2;
                from = ivec2( 0 );
            }
            else {
                to = dimensions + position;
                from = position;
            }
            
            allowNavigation = false;
            
            if ( event.getX() < from.x || event.getX() > to.x || event.getY() < from.y || event.getY() > to.y ) {
                active = false;
            }
        }
        
        // camera navigation for "PERSPECTIVE" viewport
        void mouseDownHandler( MouseEvent event ) {
            ivec2 from;
            ivec2 to;
            if ( display == FULLSCREEN ) {
                to = dimensions*2;
                from = ivec2( 0 );
            }
            else {
                to = dimensions + position;
                from = position;
            }
            
            
            if ( event.getX() > from.x && event.getX() < to.x && event.getY() > from.y && event.getY() < to.y) {
                active = true;
                allowNavigation = true;
                camUi.mouseDown( event );
            }
            else {
                allowNavigation = false;
            }
        }
        
        void mouseDragHandler( MouseEvent event ) {
            if ( active ) {
                if ( allowNavigation ) {
                    camUi.mouseDrag( event );
                }
            }
        }
        
        //////////////////////////////////////////////////////////////////////
        
        // get methods
        bool isActive() {
            return active;
        }
        
        bool getWireframe() {
            return wireFrame;
        }
        
        DISPLAY getDisplayMode() {
            return display;
        }
        
        VIEW getView() {
            return view;
        }
        
        bool getShowInterface() {
            return drawInterface;
        }
        
        bool getShowGui() {
            return showGui;
        }
        
        //////////////////////////////////////////////////////////////////////
        
        // set methods
        void setActive( bool _active ) {
            active = _active;
        }
        
        void setPosition( ivec2 _position ) {
            position = _position;
        }
        
        void setLowerLeft( ivec2 _lowerLeft ) {
            lowerLeft = _lowerLeft;
        }
        
        void setDimensions( ivec2 _dimensions ) {
            dimensions = _dimensions;
            aspectRatio = (float)dimensions.x / dimensions.y;
        }
        
        void setAspectRatio( float _aspectRatio ) {
            aspectRatio = _aspectRatio;
        }
        
        void setWireframe( bool _wireFrame ) {
            wireFrame = _wireFrame;
        }

        void setDisplayMode( DISPLAY _display ) {
            display = _display;
        }
        
        void setView( VIEW _view ) {
            view = _view;
        }
        
        void setShowInterface( bool _drawInterface ) {
            drawInterface = _drawInterface;
        }
        
        void setShowGui( bool _showGui ) {
            showGui = _showGui;
        }
        
    protected:
        CameraOrtho camOrtho;
        CameraUi camUi;
        CameraPersp camPersp;
        bool fullscreen = false;
        bool allowNavigation = false;
        
        bool drawGrid = true;
        bool drawOrigin = true;
        bool wireFrame = false;
        bool active = false;
        bool drawTitle = true;
        bool drawInterface = true;

        ivec2 lowerLeft;
        ivec2 dimensions;
        ivec2 position;
        
        float aspectRatio;
        
        gl::BatchRef horizontalPlaneBatch;
        gl::BatchRef verticalPlaneBatch;
        
        DISPLAY display;
        VIEW view;
        
        vector<string> enumNames;
        
        InterfaceGlRef gui;
        bool showGui;
    };
}
